import java.util.Stack;

class Solution {
    public int calPoints(String[] operations) {
        Stack<Integer> stack = new Stack<>();
        int sum = 0;
        
        for (String op : operations) {
            if (op.equals("+")) {
                int top = stack.pop();
                int newTop = top + stack.peek();
                stack.push(top);
                stack.push(newTop);
                sum += newTop;
            } else if (op.equals("D")) {
                int newTop = 2 * stack.peek();
                stack.push(newTop);
                sum += newTop;
            } else if (op.equals("C")) {
                sum -= stack.pop();
            } else {
                int score = Integer.parseInt(op);
                stack.push(score);
                sum += score;
            }
        }
        
        return sum;
    }
}
