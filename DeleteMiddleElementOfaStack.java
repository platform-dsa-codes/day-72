//{ Driver Code Starts
//Initial template for Java

import java.io.*;
import java.util.*;

class GFG {
    public static void main (String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        
        while(t-->0)
        {
            int sizeOfStack =sc.nextInt();
            Stack <Integer> myStack=new Stack<>();
            
            //adding elements to the stack
            for(int i=0;i<sizeOfStack;i++)
            {
                int x=sc.nextInt();
                myStack.push(x);
                
            }
                Solution obj=new Solution();
                obj.deleteMid(myStack,sizeOfStack);
                
                while(!myStack.isEmpty())
                {
                    System.out.print(myStack.peek()+" ");
                    myStack.pop();
                }
                System.out.println();
        }
        
        
    }
}
// } Driver Code Ends


//User function Template for Java

class Solution
{
    // Function to delete middle element of a stack.
    public void deleteMid(Stack<Integer> s, int sizeOfStack){
        if (s.isEmpty() || sizeOfStack <= 0) {
            return;
        }
        
        // Calculate the index of the middle element
        int midIndex = sizeOfStack / 2 + 1;
        
        // Call a helper function to delete the middle element
        deleteMidUtil(s, midIndex);
    }
    
    // Helper function to delete the middle element recursively
    private void deleteMidUtil(Stack<Integer> s, int midIndex) {
        // Base case: If midIndex is 1, remove the top element
        if (midIndex == 1) {
            s.pop();
            return;
        }
        
        // Pop the top element
        int top = s.pop();
        
        // Recur for the remaining elements
        deleteMidUtil(s, midIndex - 1);
        
        // Push the popped element back if it's not the middle element
        s.push(top);
    } 
}
